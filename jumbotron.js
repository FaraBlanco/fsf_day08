/**
 * Created by Fara Aileen on 7/4/2016.
 */
(function() {
    "use strict";
    var JumboApp = angular.module("JumboApp", []);
    var JumboAppController = function() {
        var jApp = this;
        jApp.name = "";
        jApp.email = "";
        jApp.comment = "";
        jApp.date = new Date();
        jApp.minDate = new Date();
        jApp.quantity = 2;
        jApp.process = function() {
            //Do something with the form
            //$("#thank-you.modal").modal("show");
        };
        jApp.validate = function(error) {
            //create a centralized validation
            console.info("error>> " + error);
        };
    };
    JumboApp.controller("JumboAppController", ["$log", JumboAppController]);
})();